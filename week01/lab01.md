Paratore, Gianna
IC322 - Lab 01
Wireshark Introduction

**Introduction**
This lab is initial exposure to Wireshark, a packet-sniffing application. I have used Wireshark before, but I learned more about the proper way to use it and obtain information from the data collected.

**Collaboration**
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

**Process**
I followed the "Getting Started" instructions, version 8.1. They can be found at this link: http://gaia.cs.umass.edu/kurose_ross/wireshark.php.
*I am using the trace file from the lab instructions to answer these questions.*

**Questions**
1. Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark “protocol” column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?
    DNS, HTTP, TCP, UDP, and TLSv1.2 all show up in the trace file.

2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received?
    It took 0.028885 seconds for the HTTP OK reply to be received after the HTTP GET message was sent.

3. What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)?  What is the Internet address of the computer that sent the HTTP GET message?
    The IP address of the UMass computer is 128.119.245.12. The IP address of the computer that sent the HTTP GET message is 10.0.0.44.

4. What type of Web browser issued the HTTP GET request?
    The HTTP request was issued by a Firefox Web browser.

5. What is the TCP destination port number to which this HTTP request is being sent?
    The TCP destination port that this HTTP request is being sent to is port 80.

6. Print the two HTTP messages (GET and OK) referred to in question 2 above.
    See attached PDF files.
