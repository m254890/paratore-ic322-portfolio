# Homework 02
Paratore, Gianna\
IC322 - Fall AY24\
Chapter 1: R1, R4, R11, R12, R13, R14, R18, R19

**R1.** What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?\
   Hosts are the same thing as end systems. They are the devices connected to the Internet. Hosts are traditionally considered computers, but today they can also be phones, cars, industrial machines, even refridgerators. Web servers are another example of an end system.

**R4.** List four access technologies. Classify each one as home acces, enterprise access, or wide-area wireless access.\
   Cable is one example of home access. Ethernet and WiFi are examples of enterprise access. 5G is an example of wide-area wireless access.

**R11.** Suppose there is exactly one packet switch betwen a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and receiving host are R1 and R2, respectfully. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queueing, propagation delay, and processing delay.)\
   L/R1 + L/R2

**R12.** What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a  circuit-switched network?\
   Circuit-switched networks reserve the resources (buffers, link transmission rate) needed along the path between end systems for the duration of the communication. Packet-switched networks do not reserve these resources, so the session may have to wait in a queue for access to a communication link. In TDM, each connection gets to use all of the bandwidth in short intervals of time, while FDM only allows connections to use a portion of the bandwidth but have continuous access.

**R13.** Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20% of the time.\
   **a.** When circuit switching is used, how many users can be supported?\
      10 users can be supported. If each user only transmits 20% of the time, then there can be five frames per "cycle" when transmitting one after the other (5*20 = 100% of the time). Because the users are transmitting at 1 Mbps and the link supports 2 Mbps of transmission, it can have two of the 5 user sets.\
   **b.** For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queueing delay before the 
        link if two or fewer users transmit at the same time? Why will there be a queueing delay if three users transmit at the same time?\
        If two or few users transmit at a given time, they can share the bandwidth of the link without needing to wait on each other. Two users transmitting at 1 Mbps each will amount to a total of 2 Mbps bandwidth needed, which the link can support. Any additional user trying to transmit will need to wait in queue, because the link can not spare any fraction of the bandwidth for the third user to use.\
   **c.** Find the probability that a given user is transmitting.\
      20% = 1/5\
   **d.** Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.\
      1/5 * 1/5 * 1/5 = 1/125 probability that all three are transmitting simultaneously.\
        ### TIME??? ###\

**R14.** Why will two ISPs at the same level of hierarchy often peer with each other? How does an IXP earn money?\
   ISPs at the same level of the hierarchy will peer their traffic to save costs. It is settlement-free, meaning neither ISP pays each other, but it allows their traffic to pass over direct connection rather than needing upstream intermediaries. IXPs make money by allowing multiple ISPs to peer together at their third-party "meeting point."

**R18.** How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5*10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?\
   Propagation delay is calculated generally as d/s, or distance divided by propagation speed. As such, this packet will propagate in 0.01 seconds. Propagation delay does not depend on packet length or transmission rate.

**R19.** Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rate R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.\
   **a.** Assuming no other traffic in the network, what is the throughput for the file transfer?\
      The throughput is 500 kbps, because R1 is the minimum rate (or the bottleneck link).\
   **b.** Suppose the file is 4 million bytes. Dividing the file size by throughput, roughly how long will it take to transfer the file to Host B?\
      The transfer will take about 8 seconds.\
   **c.** Repeat (a) and (b), but now with R2 reduced to 100kbps.\
      With R2 being reduced to 100kbps, this link becomes the new bottleneck link. The transfer will now take about 40 seconds.