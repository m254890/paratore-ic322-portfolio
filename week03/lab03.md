# Lab 03
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
This lab aimed to focus on familiarization with the Domain Name System, or DNS. In the lab I examined DNS query and response messages to learn more about what these messages look like and how they are sent and received to each other. I also became familiar with nslookup, a tool to help match domain names to IP addresses. This lab also helped me feel more confident using Wireshark to examine network traffic.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

## Process
I followed the DNS Lab instructions, version 8.1. They can be found at this [link](https://gaia.cs.umass.edu/kurose_ross/wireshark.php).\
*Because I do not have sudo access on my computer, I used the tracefiles provided in the lab for questions 5-18.*

## Questions
### nslookup
1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in.  What is the IP address of www.iitb.ac.in?
    - The IP address of www.iitb.ac.in is 103.21.124.10.
2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?
    - The IP address of the DNS server that gave the answer to Question 1 is 127.0.0.53.
3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?
    - The answer to Question 1 came from a non-authoritative server.
4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain.  What is that name?  (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?
    - There are three authoritative name servers for the iitb.ac.in domain. The first one is named dns1.iitb.ac.in. If I wanted to find the IP address of that name server, I would use nslookup with that domain name.

### Tracing DNS with Wireshark
*note: I do not have sudo access on my computer and thus can not flush my cache. As such, I am using the tracefiles provided in the lab.*

#### Ordinary Web Traffic
5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number6 in the trace for the DNS query message?  Is this query message sent over UDP or TCP?
    - The first DNS query message resolving to gaia.cs.umass.edu is traced to packet number 15. The query message is sent over UDP.
6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message?  Is this response message received via UDP or TCP? 
    - The corresponding DNS response is traced to packet number 17, and is also sent over UDP.
7. What is the destination port for the DNS query message? What is the source port of the DNS response message?
    - The destination port of the DNS query message and the source port of the DNS response message are the same, port 53.
8. To what IP address is the DNS query message sent?
    - The DNS query message is sent to 75.75.75.75.
9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
    - The DNS query message contains 1 "question" and 0 "answers."
10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
    - The DNS response message contains 1 "question" and 1 "answer."
11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu.  What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/?  What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address?    What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.
    - The packet number in the trace for the initial HTTP GET request for the base file Web page is 22. The packet number in the trace of the DNS query to resolve the hostname for the first HTTP request is 15. The packet number in the trace of the DNS response is 17. The packet number in the trace for the HTTP GET request for the image object is 205. The packet number number in the trace for the DNS query made to resolve the hostname for the second HTTP request is 15. Both HTTP GET requests are able to use the same DNS query because of DNS caching - the cache will store the hostname-IP address pair after querying it the first time, and so when the image object needs it, it is available in the cache and there does not need to be a separate DNS query.

#### Type A Queries
12. What is the destination port for the DNS query message? What is the source port of the DNS response message?
    - The destination port for the DNS query message and the source port of the DNS response message are the same, port 53.
13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
    - The DNS query message is sent to 75.75.75.75. Because this is the same IP address as the last example in the tracefiles, I will assume that this is the default local DNS server.
14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
    - The DNS query is a Type A query, and it contains no "answers."
15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
    - The DNS response message contains 1 "question" and 1 "answer1."

#### Type NS Queries
16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
    - The DNS query message is sent to 75.75.75.75. Because this is the same IP address as the last two examples in the tracefiles, I will assume that this is the default local DNS server.
17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?
    - The DNS query message contains 1 "question" and 0 "answers."
18. Examine the DNS response message.  How many answers does the response have?  What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?
    - The DNS response message contains 3 "answers," each containing Type NS responses for authoritative servers for the umass.edu domain. There are also 3 additional resource records returned, which contain Type A responses with the IP addresses for the ns servers.