# Lab 04
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
This lab focused on analyzing and understanding TCP traffic. In the lab I examined the capture of a file being sent over TCP to a website to understand how data is sent over TCP. This lab also helped me utilize a lot of the additional information Wireshark provides about each segment.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

## Process
I followed the TCP Lab instructions, version 8.1. They can be found at this [link](https://gaia.cs.umass.edu/kurose_ross/wireshark.php).\
*I used the tracefiles for this lab.*

## Questions
1. What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?  To answer this question, it’s probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the “details of the selected packet header" window.
    - The IP address of the client computer is 192.168.86.68, and the TCP port number is 55639.
2. What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
    - The IP address of gaia.cs.umass.edu is 128.119.245.12. It is sending and receiving TCP segments on port 80, which means it is using HTTP.
3. What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver)?
    - The raw sequence number of the TCP SYN segment is 4236649187. We can identify this segment as a SYN segment because it contains the 0x002 flag, which correlates to SYN. It also says [SYN] in the info section on Wireshark. This TCP receiver will be able to use Selective Acknowledgements because they are syncronized.
4. What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?
    - The sequence number of the SYNACK segment is 1068969752. We can identify this segment as a SYNACK segment because it contains the 0x012 flag, which correlates to SYNACK. It also says [SYN, ACK] in the info section on Wireshark. The value of the Acknowledgement field is 4236649188, which is one more than the sequence number of the SYN segment sent right before.
5. What is the sequence number of the TCP segment containing the header of the HTTP POST command? How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?
    - The sequence number of the TCP segment containing the header of the HTTP Post command is 4236649188. The payload field contains 1448 bytes, which is less than the total size of alice.txt, so not all of the data fits into this one segment.
6. Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.  
    - At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent?
        - The first segment was sent at 0.024047000 seconds from the first segment of the capture.
    - At what time was the ACK for this first data-containing segment received?
        - The ACK for this first segment was received at 0.052671000 seconds from the first segment of the capture.
    - What is the RTT for this first data-containing segment?
        - The RTT for this first segment is 0.028624 seconds.
    - What is the RTT value the second data-carrying TCP segment and its ACK?
        - The RTT for the second segment is 0.028623 seconds.
    - What is the EstimatedRTT value (see Section 3.5.3, in the text) after the ACK for the second data-carrying segment is received? Assume that in making this calculation after the received of the ACK for the second segment, that the initial value of EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation on page 242, and a value of a = 0.125.
        ```
        EstimatedRTT = (1-a)*EstimatedRtt + a*SampleRTT
        EstimatedRTT = (1-0.125)(0.028624) + (0.125)(0.028623)
        EstimatedRTT = 0.025046 + 0.00357788 = 0.02862388 seconds
        ```

7. What is the length (header plus payload) of each of the first four data-carrying TCP segments?
    - Segment 1 is 731 Bytes long. Segment 2, 3, and 4 are all 1480 bytes long. These numbers include the lengths of the headers, all of which were 20 bytes long. 
8. What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?
    - The minimum amount of available buffer space is 513, because that is the listed window size when looking at these packets. It appears that there is some throttling after the first of these four segments, most likely because all four segments have a longer length than the window. The second through fourth segments come in at the same time.
9. Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
    - Yes, the segment with sequence number one. I looked for repeated sequence numbers from the sender and this one repeated.
10. How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?
    - The amount of data acknowledged by the receiver does not seem to be consistent throughout. It waits until after the first 10 data-carrying segments to ACK the first data-carrying segment, and then waits one and ACKs the second segment. Then it waits 6 segments and ACKs two in a row. It does not seem to have a pattern.
11. What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.
    - The throughput is 115,445 bytes per second. I calculated this by dividing the size of the second data-carrying segment (and the size for pretty much every one after) by the iRTT.
12. Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.
![sequence number graph](fleetgraph)
    - These vertical sections look like TCP is in its slow start phase. From a zoomed out perspective, an exponential line is going to look a lot like a vertical linear line, and slow start grows exponentially.
13. These “fleets” of segments appear to have some periodicity. What can you say about the period?
    - The period appears to be increasing slightly between the vertical lines, but it is not super consistent.
