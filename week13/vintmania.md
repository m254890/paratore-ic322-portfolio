# VintMania!
Paratore, Gianna\
IC322 - Fall AY24

This week, Vint Cerf will be coming to speak with us. In order to get the most out of his visit, we are answering these questions beforehand. My source for this assignment is his official biography from the Internet Hall of Fame, which can be found [here](https://www.internethalloffame.org/vint-cerf/).

#### Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
Vint Cerf earned the title as "Father of the Internet" because he is the co-designer of the TCP/IP protocols as well as the architecture of the Internet itself. He and his partner Robert Kahn were awarded the U.S. National Medal of Technology in 1997 for the creation of the Internet. In 2005, Cerf and Kahn were awarded the Presidential Medal of Freedom for their work, the highest civilian award given by the United States to its citizens.

#### Find 3 surprising Vint Cerf facts.
1. Cerf is honorary chairman of the IPv6 Forum, dedicated to raising awareness and speeding introduction of the new Internet protocol.
2. His personal interests include fine wine, gourmet cooking and science fiction.
3. He has been inducted into 7 separate Hall of Fames around computer science and cybersecurity.

#### Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
1. What does "Internet Evangelist" mean to you?\
*This is Cerf's official title at Google, and has been since 2005. I am curious about what his role entails and why they picked this title. I don't know what it would mean, and with someone as important to the Internet as him I am sure it's a weighty role.*

2. What did you think about the future of the Internet as you were designing it, and how does that compare to its impact on our world now?\
*The Internet is a massive part of our every day lives, and I am curious if he knew just how integral it would be in the initial stages of creating it. I think it would be cool to hear about his vision for it while he was building it, and if it has followed that path or diverted in some way.*
