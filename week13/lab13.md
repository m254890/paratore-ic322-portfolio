Lab 13
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I revised my solution for Act II, Chapters 1 of the Protocol Pioneer game, started by LCDR Downs. This lab focused on the Link Layer rather than the Network Layer, which we have been working in throughout Act I. My previous lab has a solution that is better, but still inefficient. This week we were asked to improve upon it for a success rate of 0.3 or greater.

## Collaboration
I worked alone on this lab.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by pulling the updates using the instructions in the README.md, ran Jupyter Lab, and then played through Act II Chapters 1. The chapter continues the narrative about the story that weaves in the instructions for this chapter, and comments within the code that point you in the right direction. 

## Questions
#### 1. Include your client and server strategy code.
N/A.

#### 2. Explain your strategies in English.
I could not figure out a way to get a better rate than my lab from the previous week. All the code is linked [here](https://gitlab.com/m254890/paratore-ic322-portfolio/-/blob/master/week12/lab12.md).

#### 3. What was your maximum steady-state success rate (after 300 or so ticks?)
After 300 our rate was about 0.091, and at 450 it was at 0.105.

#### 4. Evaluate your strategy. Is it good? Why or why not?
It is not good enough for this week's assignment.

#### 5. Are there any strategies you's like to implement, but you don't know how?
I am not sure.

#### 6. Any other comments?
I started this lab a while ago and am really struggling to drop myself back into it and figure it out. I struggled with it while I was initially working on it, so it became even more impossible. I also definitely struggle with Python.
