# Lab 04
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I played the Protocol Pioneer game, started by LCDR Downs. I got to play through the first two chapters and beta tested the game. This game is supposed to open up networking and programming to people who may not be exposed to it in a fun and engaging way. Through this lab I gained more confidence in my network programming skills, problem solving skills, and brushed up on my python knowledge.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by installing Jupyter Lab using the instructions in the README.md, and then played through Chapters 1 and 2. The chapters contain narrative about the story that weave in the instructions for that chapter, and comments within the code that point you in the right direction.

## Questions
#### 1. How did you solve Chapter 1? Please copy and paste your winning strategy, and also explain it in English.
```
if self.state["ticks"] % 2 == 0:
        # self.send_message("Mayday!", "N")
        self.send_message("Message Received", "W")
```
When running the cells for the first time, we can see a message from the mothership telling us to respond "Message Received" if we get their message. We can also see it is coming from the west interface, so we must send our message back through that interface. I changed the part where we send out the Mayday message to send this confirmation message to the mothership through the west interface, and that worked.

#### 2. How did you solve Chapter 2? Again, copy and paste your winning strategy and explain it.
```
while(self.message_queue):
        m = self.message_queue.pop()
        newNum = m.text
        self.state["received"][m.interface][0] = self.state["received"][m.interface][0] + 1
        self.state["received"][m.interface][1] += (int)(newNum)
        sending = self.state["received"][m.interface][1]
        print(f"Msg on interface {m.interface}: {m.text} (sum = {sending})")
        if self.state["received"][m.interface][0] == 3:
            sending = self.state["received"][m.interface][1]
            self.send_message(f"{sending}", m.interface)
            print(f"Sending {sending} to {m.interface}")
```
The instructions for this chapter were very helpful, and I appreciated the example in the narrative. The comments also helped me understand the state array where we are saving these messages. I added to the while loop that pops each message from the queue. For each message, it will go to the spot in the "received" part of the array for that interface. It will add the number received to the sum stored at index 1, and incrememnt the message counter stored at index 0. If the counter equals three, then we will send a message with the sum for that interface to the interface, and also prints the message it is sending so that we can see what is happening under the hood.

#### 3. Include a section on Beta Testing Notes.
**a. List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.**\
    I didn't really find any bugs in the program. It worked well for me.\
**b. Was there anything that you and others were consistently confused about? What would you do to solve that confusion?**\
    One thing I was confused about was how to open jupyter lab in the right folder. That may have just been me, but I struggled to get it to open where I could see the chapters, it was just opening where the jupyter lab application itself lived. Adding something about this to the README might be helpful. I also felt that the first chapter did not have enough explanation on wht exactly we were expected to do. Looking at it for the first time there was already a lot of code, so it seemed like the work was already done. After a short conversation with LCDR Downs I was able to figure out where I should change things. However if I was playing this on my own, especially if I was new to coding, I would have been very confused and probably discouraged. I would maybe include either more comments on the first chapter, or perhaps a Chapter 0 that helps the player understand the spaceship "console," or maybe another fun character like a robot AI or a trusty sidekick that can point you in the right direction at the beginning.\
**c. Was there too much narrative? Not enough narrative?**\
    I actually think the narrative is the right ratio. I appreciate the story behind it, it makes the game a lot more engaging.\
**d. Was it fun?**\
    I thought it was fun! It might be challenging for someone who has not seen a lot of python or coding before, and maybe an option could be included for tutorials or more explanations (like "beginner" and "advanced" modes). I especially liked the moving diagram with the spaceships and the packets. It really helped me visualize what we were doing, and I was impressed with how it looked.