# MIDN 2/C Paratore IC322 Portfolio

I'm maintaining this Portfolio for IC322: Computer Networks. If you're visiting, welcome! Feel free to leave feedback by opening an issue or submitting a merge request.

**Assignments:**\
Week 1: [The Internet](/week01/)\
Week 2: [The Application Layer - HTTP](/week02/)\
Week 3: [The Application Layer - DNS and Other Protocols](/week03/)\
Week 4: [The Transport Layer - TCP and UDP](/week04/)\
Week 5: [The Transport Layer - Part 2](/week05/)\
Week 7: [The Network Layer - Data Plane](/week07/)\
Week 8: [The Network Layer - More Data Plane](/week08/)\
Week 9: [The Network Layer - Control Plane](/week09/)\
Week 10: [The Network Layer - More Control Plane](/week10/)\
Week 12: [The Link Layer](/week12/)\
Week 13: [The Link Layer - Part 2](/week13/)\
Week 15: [TLS and Network Security](/week15/)
