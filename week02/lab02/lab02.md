# Lab 02
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
This lab focused on socket programming in python and creating a server that could send files requested from the browser. Using HTTP messages, the browser client would request a file and my server should send the file back with the right HTTP response for the file type. If the file did not exist, it should send the proper "404 Not Found" error.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates. For Part 4 we were encouraged to try out LLM and see how it supports our programming abilities, and I used ChatGPT to help guide me just for Part 4.

## Process
I followed the lab instructions from our course website, which can be found [here](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/blob/main/assignments/week02/lab-build-a-server.md?ref_type=heads). I started with the code from Section 2.7 of the textbook as a base, and then followed through the lad online. I tried my best on Part 2 but I just could not get it to work completely, so I am not positive that my later work functions either. As mentioned in the Collaboration section, I used assistance from ChatGPT on Part 4.

## How to Run the Server
1. Download TCPServer.py
2. Type `python3 TCPServer.py` into the command line and wait for the ready message
2. Navigate to `http://localhost:12000/index.html` in your browser
4. Request your file! Make sure you have one in the same directory you saved TCPServer.py to.