from socket import *
import sys # so can terminate program

serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)

serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print('The server is ready to receive')

while True:
    print('Ready to serve...')
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(1024).decode()
    # capitalizedSentence = sentence.upper()
    # connectionSocket.send(capitalizedSentence.encode())
    # connectionSocket.send(sentence.encode())
    message = '<html><body>Hello there!</body></html>'
    connectionSocket.send(message.encode())
    try:
        message = sentence
        filename = message.split()[1]
        line1 = 'HTTP/1.1 200 OK\r\n'
        line2 = 'Content-Type: '
        if (filename.endswith('.jpg')):
            line2+= 'JPG'
            with open(filename, 'rb') as file:
                image_data = file.read()
                connectionSocket.send(line1.encode())
                connectionSocket.send(line2.encode())
                connectionSocket.send(str(len(image_data)).encode())  # Send image size
                connectionSocket.send(image_data)
        elif (filename.endswith('.png')):
            line2+= 'PNG'
            with open(filename, 'rb') as file:
                image_data = file.read()
                connectionSocket.send(line1.encode())
                connectionSocket.send(line2.encode())
                connectionSocket.send(str(len(image_data)).encode())  # Send image size
                connectionSocket.send(image_data)
        else: # NORMAL FILES
            f = open(filename[1:])
            outputdata = f.read()
        #Send one HTTP header line into socket
            
            connectionSocket.send(line1.encode())
        #Send the content of the requested file to the client
            for i in range(0, len(outputdata)):
                connectionSocket.send(outputdata[i].encode())
                connectionSocket.send("\r\n".encode())

        connectionSocket.close()
    except IOError:
    #Send response message for file not found
        fnf = 'HTTP/11 404 Not Found'
    #Fill in start
    #Fill in end
    #Close client socket
        connectionSocket.close()
    #Fill in start
    #Fill in end
        serverSocket.close()
        sys.exit()#Terminate the program after sending the corresponding data 

    # print(sentence)
        connectionSocket.close()