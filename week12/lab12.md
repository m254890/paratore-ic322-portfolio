Lab 12
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I played through Act II, Chapters 1 of the Protocol Pioneer game, started by LCDR Downs. This lab focused on the Link Layer rather than the Network Layer, which we have been working in throughout Act I. It has a solution that is inefficient, and asks us to improve upon it for a higher success rate.

## Collaboration
This lab was a partner lab, and I worked with Jimmy Flanagan.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by pulling the updates using the instructions in the README.md, ran Jupyter Lab, and then played through Act II Chapters 1. The chapter continues the narrative about the story that weaves in the instructions for this chapter, and comments within the code that point you in the right direction. 

## Questions
#### 1. Include your client and server strategy code.
Client Strategy:
```
def client_strategy(self):

    if "go_time" not in self.state:
        self.state["go_time"] = 0
    
    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Client {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)
    
    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    if not self.interface_sending(selected_interface) and self.state["go_time"] < self.current_tick():
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            # if not self.interface_receiving(selected_interface):
            self.send_message(msg_text, selected_interface)

            #wait 10 ticks after sending
            self.state["go_time"] = self.current_tick() + 10
```
Server Strategy:
```
def server_strategy(self):
    if "go_time" not in self.state:
        self.state["go_time"] = 0
    
    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Server {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)


    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    if not self.interface_sending(selected_interface) and self.state["go_time"] < self.current_tick():
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            # if not self.interface_receiving(selected_interface):
            #     print(f"{self.id}: Attempting send.")
            #     self.send_message(msg_text, selected_interface)
            self.send_message(msg_text, selected_interface)

            #wait 10 ticks after sending
            self.state["go_time"] = self.current_tick() + 10
```

#### 2. Explain your strategies in English.
Our client strategy was to send a message, and then create a "go time" that will have the sender wait 10 ticks between sending. The messages it receives will be read out, but once the ticks have gotten past that "go time" then it will try sending the next one.\
Our server strategy is similar. It will send messages using the same 10 tick "go time" to avoid collisions as much as possible.

#### 3. What was your maximum steady-state success rate (after 300 or so ticks?)
After 300 our rate was about 0.091, and at 450 it was at 0.105.

#### 4. Evaluate your strategy. Is it good? Why or why not?
I think our strategy is alright, it is better than the starting rate, but it could definitely be better. I am not sure how to improve it though.

#### 5. Are there any strategies you's like to implement, but you don't know how?
Honestly, I do not remember what might make it better, it has been a while since completing this lab.

#### 6. Any other comments?