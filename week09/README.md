# Week 9: The Network Layer - Control Plane

This week we started focusing on new protocols in the control plane, and continued with Protocol Pioneer.

This week I left feedback on Ryan Schleicher's portfolio for Week 8. That feedback can be found [here](https://gitlab.com/RyanSchleicher11/ic322-portfolio/-/issues/15).
