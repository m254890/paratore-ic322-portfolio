Lab 09
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I played the Protocol Pioneer game, started by LCDR Downs. I got to play through Chapter 5 and beta tested the game. This game is supposed to open up networking and programming to people who may not be exposed to it in a fun and engaging way. Through this lab I gained more confidence in my network programming skills, problem solving skills, and brushed up on my python knowledge.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by pulling the updates using the instructions in the README.md, ran Jupyter Lab, and then played through Chapter 5. The chapter continues the narrative about the story that weaves in the instructions for this chapter, and comments within the code that point you in the right direction.

## Questions
#### 1. How did you solve Chapter 5? Please copy and paste your winning strategy, and also explain it in English.
Chapter 5 requires three methods: the drones (routers), your character (source), and Sawblade(destination). I started with my character and decided how to handle sending messages so that they would not cause keys to expire before they reached Sawblade. Since the expiration window of each key is 18 ticks, I chose to only send one message every 18 ticks so that it would not cause any keys on the network that are still valid to expire early and break Sawblade's radio method.
```
def player_strategy(self):
    """This is you. You have access to the key-generating machine. You need to send that key to Sawblade.
    Your id is "1"
    """
    # `self.generate_key()` generates a new key. The function returns a tuple: (key, expiration). The first value
    # is the actual key (a number). The second value is the time that the key expires.
    # Every time you call the function, a new key is generated and the expiration date is set 18 ticks into the future.
    

    # If you want, you can use your heartbeat to count ticks.
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    print(f"The current time is {self.state['ticks']}...")

    #this way never send message until last message expires
    if (self.state['ticks']%18 == 1): #1 so send at first tick
        key_expiration = self.generate_key()
        key = key_expiration[0]
        expiration = key_expiration[1]
        print(f"Player: key {key} expires at {expiration}.")
        self.send_message(f"PLAYER key: {key} expires: {expiration}", "E")
```
I next determined how I would send the message across the drone network. Because we know the direction Sawblade is in (down and to the right), I instructed the drones to always try to send the message south first. If they do not have a south interface, I have the drone send the message east. Using this method, we know that the message will eventually get to Sawblade in less than 18 seconds, so that it gets to her before the key expires.
```
def drone_strategy(self):
    """Drones are responsible for routing messages."""

    while(self.message_queue):
        m = self.message_queue.pop()
        # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
        number_of_connected_interfaces = len(self.connected_interfaces())
        print(f"Drone {self.id} has {number_of_connected_interfaces} connected interfaces: {self.connected_interfaces()}")
        if ("S" in self.connected_interfaces()):
            self.send_message(f"{m.text}", "S")
        else:
            self.send_message(f"{m.text}", "E") #get all the way down if can, and keep pushing right when cant!
```
My final step was to figure out how Sawblade would receive the message and use the key to call the radio method. This was fairly easy, I just had to split the message to extract only the number value of the key, and then use that to call the radio. Once the first message made it to Sawblade, I saw this all worked!
```
def sawblade_strategy(self):
    """Sawblade operates the radio, but need an unexpired key.
    Sawblade's id is "2"
    """

    # Sawblade has the `self.operate_radio(key)` method available to her. Call it with the value of the key that the
    # player has. Don't use an expired key, the escape pod will go on permanent lock down!
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Sawblade: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        key = m.text.split(" ")[2] #number for key
        self.operate_radio(key)
```

#### 2. Include a section on Beta Testing Notes.
**a. List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.**\
None.\
**b. Was there anything that you and others were consistently confused about? What would you do to solve that confusion?**\
I think there should be clarification on why IDs are needed. I did not feel like there was a need for them, unless we were expected to mimic a real-life routing protocol with header information (source and destination). If that was the case that probably would have been manageable, I just would have liked a comment or piece of text somewhere indicating this. I couldn't figure out why we would need them, so I felt a bit stumped trying to figure out how to answer the problem by using them since they seemed (and were) unnecessary.\
**c. Was there too much narrative? Not enough narrative?**\
I wished there was a little more at the end of this chapter. I liked the section at the beginning, but at the end I couldn't tell if I had actually won or not. I could tell a bit from the story that I was safe, but it would have been helpful to just include a "GOOD WORK" or some indication that we had beat or failed the chapter.\
**d. Was it fun?**\
Yes! I liked this chapter and the more small functions needed, as compared to the big long ones in Chapter 4.