# Man in the Browser Attacks

## Contents
1. Introduction
1. How the Attack Works
1. Executing the Attack
1. tejhleker
1. Ethical Implications
1. Legal Considerations
1. Professional Responsibilities

## Introduction
What is man in the middle?

## How the Attack Works
So what actually is a Man in the Browser attack? It is like a man in the middle attack, but specifically one that exploits a web browser. This can be done in one of three ways:\
**Compromised browser extensions:** browser extensions are tools that can be added to a browser to provide additional functionality. Typically they make HTTP POST or GET requests for the user, and they are often vulnerable to malicious code injection.\
**API hooking:** some browsers have necessary API calls in order to be able to send and receive HTTP requests. The perpetrator acts as a man in the middle between an API and the process requesting it, thus hijacking these HTTP messages.\
**Malicious content scripts:** content scripts are code that runs in context of a web page and are able to read and manipulate pages. These scripts can be injected onto a site by infecting the server hosting the site or infecting the HTTP response to the client using man in the middle techniques.\
All three of these avenues require exploitation of computer networks, particularly the application layer. Man in the Browser Attacks are particularly tricky to catch and mitigate because they are a client side attack. This sometimes enables them to get grandfathered into all the client's security measures and the server will not be able to tell anything is wrong.

## Executing the Attack
Attackers can use the Browser Exploitation Framework, or BeEF, to carry out the attack. BeEf is an open-source pen-testing tool like Metasploit, but specifically for browsers.

#### Step 1: Setup
The first step of the attack is initializing the BeEF server. BeEF will give you malicious JavaScript code that can be injected into the victim browser. The attacker then logs into the control panel in the browser using the BeEF user interface.

#### Step 2: Hooking
The next part is to inject that JavaScript code to the browser. This can be done with:
1. a traditional man in the middle attack
2. cross sight scripting (XSS)
3. social engineering (like phishing)
Once the code has been injected, when the victim accesses the web page it will become visible to the attacker on BeEF. This is called "hooking," because the attacker catches onto the browser and can see and stay in the browser.

#### Step 3: Fingerprinting
The next step is to fingerprint the victim. The attacker can discover LAN devices by looking at the default logo images in the User Interface. BeEF also allows the attacker to narrow their search for target IP ranges or ports. They can also fingerprint the browser using a tool called FingerPrintJS2. This lets the attacker understand the capabilities of the browser that they hooked onto. BeEF has even more modules that allow the attacker to learn more about the connection they have made.

#### Step 4: Establish Persistence
It is beneficial for the malicious actor to establish persistence, or a sure footing in the browser they have hooked. This allows them to continue to exploit the connection they have made to the victim without having to repeat steps 1 and 2 every time. BeEF can detect antivirus software by searching for JavaScript code that is known to be automatically included in the softwares. This allows the attacker to understand what security measures are installed on the host they are attacking. Some of the known softwares that can be detected are Kapersky, Avira, Avast, Norton, and more.\
The man-in-the-browser attack injects a hook initially into a single web page. In this case, leaving that page should bring the victim to safety. To sidestep this, BeEF will listen for link clicks on the site and handle the requests, instead of letting the browser do it. Like a traditional man-in-the-middle attack the attacker protects their connection by silently making requests for the victim and masquerading to the server using the victim's security measures. Some servers will prevent this if the link clicked is to a different domain, so BeEF will open the links in a new tab to keep the hook alive. The hook will only be lost if the victim manually types and changes the URL. There are even more options for establishing persistence with other modules in BeEF.

#### Step 5: Data Collection
Now that their connection is secured, the attacker can gather data on the victim through the exploited browser. Some of these options include:
**social media:** BeEF can detect if the browser is authenticated to Gmail, Facebook, and Twitter. This is a HUGE problem because of how much personal data is available through these sites, or any business information that may be communicated via email.\
**spider eye:** this feature allows the attacker to capture a snapshot of the hooked web page.\
**form values:** the attacker can get the name, type, and value of ALL input fields on the hooked web page. This is very dangerous if any passwords or sensitive data is being input.

#### Additional Attack: Data Manipulation
By sending its requests from the victim's browser, an attacker can execute additional attacks. Once hooked, the attacker can actually take advantage of the victim's security context, because the victim's computer will do all the authentication to a service. BeEF's communication channel will tunnel the attacker's request to the hooked browser, similar to a reverse HTTP proxy where the hook is the endpoint. The attacker can also visually hijack a browser to see whatever the victim sees on their screen. This allows them to bypass logins. After the victim logs in and a proxy has been set up, when the attacker accesses that web page they will not need to log in because they are browsing "as" the victim with the victim's security measures already in place.

## Ethical Implications
Discuss the ethical implications of the vulnerability/attack. Consider aspects such as privacy, data integrity, and the potential impact on individuals and organizations. Reflect on how professionals in the field should ethically respond to such vulnerabilities.

## Legal Considerations
Analyze the legal aspects related to your chosen topic. This may include laws and regulations that apply to the exploitation or prevention of such vulnerabilities/attacks. Discuss how legal frameworks guide professional practices in cybersecurity.

## Professional Responsibilities
Examine the responsibilities of computing professionals in the context of your topic. Discuss how professionals can make informed decisions that align with ethical principles and legal obligations in their response to such security challenges.
As computer professionals and users, it is important to protect our systems against known threat vectors to the best of our abilities. There are a few categories of solutions that people should take.

#### Server-Side Mitigations
Server systems should expand out-of-band authentication so that the user is sent a summary of the actions performed. This would show all the actions they took, but also any requests made by the attacker.
- expand out-of-band authentication where user is sent summary of actions performed
    - these would show our attacking requests!
- these may be important for certain kinds of transactions!
    - ex. online funds transferring, client may have to confirm with out-of-band one time password, this will be sent in email which includes details about transaction

#### Client-Side Mitigations
- malware detection software, many MIDB malwares contain static code
    - these can be caught/signed, blocked, and removed by antivirus
    - as we saw though, the attacker still checks for this!
- user training: identify suspicious extensions and social engineering attempts
    - here specifically, close browser sessions regularly

#### General Networking Best Practices
- Segregate network with subnets/VLANS
- regulate access to internet w/ proxy server, controls and monitors user behavior
- place security devices, correctly - firewall at every junction not just edge, anti DDoS devices at network edge, cloud services at network edge
- NAT provides extra layer of security
- monitor network traffic, combine different security tools to cover blind spots, many attacks span multiple accounts or vectors
- deception technology, creates decoys across network, allows you to observe the attacker's plans and techniques, can set up decoy data, credentials, or network connections

## Sources
https://www.cynet.com/attack-techniques-hands-on/man-in-the-browser-attacks/#heading-1\
https://www.cynet.com/network-attacks/network-attacks-and-network-security-threats/
