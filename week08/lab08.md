Lab 08
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I played the Protocol Pioneer game, started by LCDR Downs. I got to play through the Chapters 3 and 4 and beta tested the game. This game is supposed to open up networking and programming to people who may not be exposed to it in a fun and engaging way. Through this lab I gained more confidence in my network programming skills, problem solving skills, and brushed up on my python knowledge.

## Collaboration
I worked through this lab on my own, and did not receive any assistance from classmates or outside sources.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by installing Jupyter Lab using the instructions in the README.md, and then played through Chapters 1 and 2. The chapters contain narrative about the story that weave in the instructions for that chapter, and comments within the code that point you in the right direction.

## Questions
#### 1. How did you solve Chapter 3? Please copy and paste your winning strategy, and also explain it in English.

#### 1. How did you solve Chapter 4? Please copy and paste your winning strategy, and also explain it in English.

#### 3. Include a section on Beta Testing Notes.
**a. List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.**\
**b. Was there anything that you and others were consistently confused about? What would you do to solve that confusion?**\
- little python technicalities: didn't know self.id was a string, kept running into problems until i thought to try that.
**c. Was there too much narrative? Not enough narrative?**\
**d. Was it fun?**\