Lab 10
Paratore, Gianna\
IC322 - Fall AY24

## Introduction
For this lab I updated my answers for Chapter 5 of the Protocol Pioneer game, started by LCDR Downs. This lab asked us to slow down and confirm that our code is accurate and applicable to the concepts we are learning in class.

## Collaboration
I worked through this lab on my own. I used the instructor solution to help me with my answer.

## Process
This lab is hosted on a Jupyter Notebook, which can be found on the class gitlab [here](https://gitlab.com/jldowns-usna/protocol-pioneer). I started by pulling the updates using the instructions in the README.md, ran Jupyter Lab, and then played through Chapter 5 again. The chapter continues the narrative about the story that weaves in the instructions for this chapter, and comments within the code that point you in the right direction.

## Questions
For this chapter, I was very confused the first time I did it about how to apply the things we were learning in class. For the most part, the instructor solution makes sense, I just don't know how I would have come up with that without seeing it first. One bug I think I caught was in Sawblade's strategy, when checking if the key is expired it says `if t <= t`. This will always be true, since it is comparing the same thing to itself. For the program to function the way it is intended, I think it is supposed to be `if t <= e`, meaning that there have been less ticks than the expiration of that key. 