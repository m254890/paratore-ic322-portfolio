# Week 10: The Network Layer - More Control Plane

This week we continued working on the control plane, and tightened up our code for Protocol Pioneer.

This week I left feedback on Peter Asjes' portfolio for Week 9. That feedback can be found [here](https://gitlab.com/PJAsjes/ic322-portfolio/-/issues/17).
