# Week 5: Transport Layer - Part 2

We did not have a lab this week, instead we focused on building a rubric as a class.

This week we were asked to review the Week 4 work of one of our classmates. I left my peer feedback this week for Jimmy Flanagan. The issue on his portfolio is linked [here](https://gitlab.usna.edu/m251740/ic322/-/issues/8).
